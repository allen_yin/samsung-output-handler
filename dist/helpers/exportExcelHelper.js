'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

var _exceljs = require('exceljs/dist/es5/exceljs.browser');

var _exceljs2 = _interopRequireDefault(_exceljs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var LOG = (0, _debug2.default)('output-handler-helpers');

var getExcelColumnName = function getExcelColumnName(num) {
  for (var ret = '', a = 1, b = 26; (num -= a) >= 0; a = b, b *= 26) {
    ret = String.fromCharCode(parseInt(num % b / a) + 65) + ret;
  }
  return ret;
};

var prepareExcelWorkbookFromArray = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(params) {
    var workbooks, workbook, wbName;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            workbooks = params.workbooks;
            workbook = new _exceljs2.default.Workbook();
            _context.t0 = regeneratorRuntime.keys(workbooks);

          case 3:
            if ((_context.t1 = _context.t0()).done) {
              _context.next = 10;
              break;
            }

            wbName = _context.t1.value;

            if (workbooks.hasOwnProperty(wbName)) {
              _context.next = 7;
              break;
            }

            return _context.abrupt('continue', 3);

          case 7:
            workbook = prepareExcelSheetFromArray({
              autoFilter: workbooks[wbName]['autoFilter'],
              columns: workbooks[wbName]['columns'],
              data: workbooks[wbName]['data'],
              heights: workbooks[wbName]['heights'],
              mergeCells: workbooks[wbName]['mergeCells'],
              name: wbName,
              styles: workbooks[wbName]['styles'],
              views: workbooks[wbName]['views'],
              workbook: workbook
            });
            _context.next = 3;
            break;

          case 10:
            return _context.abrupt('return', workbook);

          case 11:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined);
  }));

  return function prepareExcelWorkbookFromArray(_x) {
    return _ref.apply(this, arguments);
  };
}();

function prepareExcelSheetFromArray(params) {
  var autoFilter = params.autoFilter,
      columns = params.columns,
      data = params.data,
      heights = params.heights,
      mergeCells = params.mergeCells,
      name = params.name,
      styles = params.styles,
      views = params.views,
      workbook = params.workbook;

  var sheet = workbook.addWorksheet(name);
  LOG('sheet ' + name + ' added....');
  sheet.autoFilter = autoFilter;
  LOG('autoFilter added.....');
  sheet.columns = columns;
  LOG('columns added.....');
  sheet.addRows(data);
  LOG('rows added.......');
  sheet.views = views;
  styles !== undefined && Object.keys(styles).forEach(function (cellKey) {
    sheet.getCell(cellKey).numFmt = styles[cellKey].numFmt;
    sheet.getCell(cellKey).font = styles[cellKey].font;
    sheet.getCell(cellKey).alignment = styles[cellKey].alignment;
    sheet.getCell(cellKey).border = styles[cellKey].border;
    sheet.getCell(cellKey).fill = styles[cellKey].fill;
  });

  mergeCells !== undefined && Object.keys(mergeCells).forEach(function (merge) {
    sheet.mergeCells(merge);
  });
  heights !== undefined && heights.forEach(function (rowConfig) {
    sheet.getRow(rowConfig.row).height = rowConfig.height;
  });
  LOG('workbook ready');
  return workbook;
}

var subHeaderStyle = {
  // font: {
  //   bold: true,
  // },
  alignment: {
    vertical: 'middle',
    horizontal: 'center'
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } }
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00F6F7F8' }
  }
};

var topHeaderStyle = {
  font: {
    size: 13,
    bold: true
  },
  alignment: {
    vertical: 'middle',
    horizontal: 'center'
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } }
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00F6F7F8' }
  }
};

var evenCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center'
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } }
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00F6F7F8' }
  }
};

var oddCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
    wrapText: true
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } }
  }
};

var warningCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center'
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } }
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00FFFF00' }
  }
};

var dangerCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center'
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } }
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00EF5350' }
  }
};

var goodCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center'
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } }
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '0000C853' }
  }
};

var greatCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center'
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } }
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '0029B6F6' }
  }
};

var styles = exports.styles = {
  subHeaderStyle: subHeaderStyle,
  topHeaderStyle: topHeaderStyle,
  evenCellStyle: evenCellStyle,
  oddCellStyle: oddCellStyle,
  warningCellStyle: warningCellStyle,
  dangerCellStyle: dangerCellStyle,
  goodCellStyle: goodCellStyle,
  greatCellStyle: greatCellStyle
};

var exportXLSXHelper = {
  getExcelColumnName: getExcelColumnName,
  prepareExcelWorkbookFromArray: prepareExcelWorkbookFromArray
};

exports.default = exportXLSXHelper;