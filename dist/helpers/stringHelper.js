'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.safelyDecodeString = exports.ipStringValidate = exports.jsonStringWithoutQuotes = exports.getUrlParam = exports.strEllipsis = exports.capFirstLetter = exports.getCapitalizedString = exports.defaultDashValue = exports.replaceAll = exports.getFileType = exports.fileNameDecoding = exports.fileNameEncoding = exports.fileNameAndExtension = exports.phoneNumberFormat = exports.toUnderscore = exports.removeLastChar = exports.capitalize = undefined;

var _attachment = require('../components/constants/attachment');

var capitalize = exports.capitalize = function capitalize(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
};

var removeLastChar = exports.removeLastChar = function removeLastChar(str) {
  if (str.length > 0) {
    return str.substr(0, str.length - 1);
  }
};

var toUnderscore = exports.toUnderscore = function toUnderscore(str) {
  if (str && str.length > 1) {
    String.prototype.toUnderscore = function () {
      return this.replace(/([A-Z])/g, function ($1) {
        return '_' + $1.toLowerCase();
      });
    };

    return str.toUnderscore();
  }
};

var phoneNumberFormat = exports.phoneNumberFormat = function phoneNumberFormat(num) {
  var numVal = num ? num : '';
  var numStr = numVal.split(' ').join('').split('-').join('').split('(').join('').split(')').join('');

  if (numStr.length !== 10 || !/^\d+$/.test(numStr)) return '';else return '(' + numStr.slice(0, 3) + ') ' + numStr.slice(3, 6) + '-' + numStr.slice(6, numStr.length);
};

var fileNameAndExtension = exports.fileNameAndExtension = function fileNameAndExtension() {
  var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

  var arr = name.split('.');
  var fileName = arr.slice(0, arr.length - 1).join('.');
  var extension = arr[arr.length - 1];
  return {
    fileName: fileName,
    extension: extension
  };
};

var fileNameEncoding = exports.fileNameEncoding = function fileNameEncoding() {
  var fileName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

  return fileName.split('.').join(_attachment.DOT_SPLITER).split(' ').join(_attachment.SPACE_SPLITER);
};

var fileNameDecoding = exports.fileNameDecoding = function fileNameDecoding() {
  var fileName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

  return fileName.split(_attachment.DOT_SPLITER).join('.').split(_attachment.SPACE_SPLITER).join(' ');
};

var getFileType = exports.getFileType = function getFileType() {
  var fileId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

  var arr = fileId.split('.');
  return arr[arr.length - 1];
};

var replaceAll = exports.replaceAll = function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
};

var defaultDashValue = exports.defaultDashValue = function defaultDashValue(value) {
  return value || value === 0 || value === false ? value : '-';
};

var getCapitalizedString = exports.getCapitalizedString = function getCapitalizedString() {
  var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

  str = str.trim();
  var result = '';
  var arr = str.split(' ');
  arr.forEach(function (item) {
    result += capitalize(item.toLowerCase()) + ' ';
  });
  return result;
};

var capFirstLetter = exports.capFirstLetter = function capFirstLetter(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
};

var strEllipsis = exports.strEllipsis = function strEllipsis(str, maxLen) {
  var trunc = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : maxLen - 2;
  return str && str.length > maxLen ? str.substring(0, trunc) + '...' : str;
};

var getUrlParam = exports.getUrlParam = function getUrlParam() {
  var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

  url = url.split('?').join('');
  var paramsArr = url.split('&');
  var params = {};
  paramsArr.forEach(function (param) {
    var arr = param.split('=');
    params[arr[0]] = arr[1];
  });
  return params;
};

var jsonStringWithoutQuotes = exports.jsonStringWithoutQuotes = function jsonStringWithoutQuotes() {
  var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  return JSON.stringify(obj).replace(/"(\w+)"\s*:/g, '$1:');
};

var ipStringValidate = exports.ipStringValidate = function ipStringValidate() {
  var str = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

  if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(str)) return true;
  return false;
};

var safelyDecodeString = exports.safelyDecodeString = function safelyDecodeString(note) {
  try {
    return decodeURIComponent(note);
  } catch (e) {
    return note;
  }
};