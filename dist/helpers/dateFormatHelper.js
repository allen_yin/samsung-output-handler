'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.exportDateFormat = exports.formatDateByNumber = undefined;

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var formatDateByNumber = exports.formatDateByNumber = function formatDateByNumber(date) {
  if (date) return (0, _moment2.default)(date, 'YYYYMMDD').format('MMM DD, YYYY');
  return 'N/A';
};

var exportDateFormat = exports.exportDateFormat = function exportDateFormat(date) {
  if (date) return (0, _moment2.default)(date, 'YYYYMMDD').format('YYYY-MM-DD');
  return 'N/A';
};