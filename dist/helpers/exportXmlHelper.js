'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

var _xmlJs = require('xml-js');

var _xmlJs2 = _interopRequireDefault(_xmlJs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LOG = (0, _debug2.default)('output-handler-helpers');

var nestElement = function nestElement(obj) {
  var res = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

  Object.keys(obj).forEach(function (key) {
    res.push({
      key: '{"' + key + '":' + obj[key] + '},'
    });
    if (obj[key]) {
      nestElement(obj[key], res);
    }
  });
  return res;
};
var attributeGroup = function attributeGroup(obj) {
  var res = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

  Object.keys(obj).forEach(function (key) {
    if (key === "AttributeGroup") {
      res.push({
        key: '{"' + key + '":' + obj[key] + '},'
      });
      if (obj[key]) {
        nestElement(obj[key], res);
      }
    } else {
      res;
    }
  });
  return res;
};

exports.default = function (jsonData) {
  if (jsonData) {
    var structuredJson = {
      "?xml": {
        "_attributes": {
          "version": jsonData.version || "1.0",
          "encoding": jsonData.encoding || "UTF-8",
          "standalone": jsonData.standalone || "yes"
        }
      },
      "Feed": {
        "_attributes": {
          "language": jsonData.language || "en",
          "market": jsonData.market || "us"
        },
        "Product": {
          "PartNumber": jsonData.partNumber || "</PartNumber>",
          "ProductDescription": jsonData.shortDescription || "</ProductDescription>",
          "UpcEan": jsonData.upcEan || "</UpcEan>",
          "LaunchDate": jsonData.launchDate || "</LaunchDate>",
          "EndOfLifeDate": jsonData.endOfLifeDate || "</EndOfLifeDate",
          "HeroImage": jsonData.largeImage || "",
          "Images": [nestElement(jsonData.images || "", [])],
          "EnergyLabelImages": [nestElement(jsonData.energyLabelImages || "", [])],
          "LogoItems": [nestElement(jsonData.logoItems || "", [])],
          "KeySellPoints": [nestElement(jsonData.keySellPoints || "", [])],
          "ProductFeatures": [nestElement(jsonData.productFeatures || "", [])],
          "PdfProductDataSheet": jsonData.pdfProductDataSheet || "</PdfProductDataSheet>",
          "PdfUserManual": jsonData.pdfUserManual || "</PdfUserManual>",
          "PdfQuickStartGuide": jsonData.pdfQuickStartGuide || "</PdfQuickStartGuide>",
          "WarrantyCard": jsonData.warrantyCard || "</WarrantyCard>",
          "InstallationGuide": jsonData.installationGuide || "</InstallationGuide>",
          "PdfFTCEnergyGuide": jsonData.pdfFTCEnergyGuide || "</PdfFTCEnergyGuide>",
          "PdfFTCLightingFacts": jsonData.pdfFTCLightingFacts || "</PdfFTCLightingFacts>",
          "ProductBrochures": jsonData.productBrochures || "</ProductBrochures>",
          "PdfMsds": jsonData.pdfMsds || "</PdfMsds>",
          "PdfEUEnergyLabel": jsonData.pdfEUEnergyLabel || "</PdfEUEnergyLabel>",
          "AccessoriesPartNumbers": [nestElement(jsonData.accessoriesPartNumbers) || "", []],
          "MainProductsPartNumbers": [nestElement(jsonData.mainProductsPartNumbers) || "", []],
          "Videos": [nestElement(jsonData.videos) || "", []],
          "Footnotes": [nestElement(jsonData.footnotes) || "", []],
          "LegalCopy": [nestElement(jsonData.legalCopy) || "", []]

        }
      }
    };
    var options = { compact: true, ignoreComment: true, spaces: 4 };
    var resultXml = _xmlJs2.default.json2xml(structuredJson, options);
    return resultXml;
  }
  return "";
};