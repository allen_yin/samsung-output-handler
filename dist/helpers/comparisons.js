"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var undefOrNull = exports.undefOrNull = function undefOrNull(thing) {
  return thing === undefined || thing === null;
};
var notUndefOrNull = exports.notUndefOrNull = function notUndefOrNull(thing) {
  return !undefOrNull(thing);
};