'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var subHeaderStyle = exports.subHeaderStyle = {
  font: {
    bold: true
  },
  alignment: {
    vertical: 'middle',
    horizontal: 'center'
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } }
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00F6F7F8' }
  }
};

var topHeaderStyle = exports.topHeaderStyle = {
  font: {
    size: 13,
    bold: true
  },
  alignment: {
    vertical: 'middle',
    horizontal: 'center'
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } }
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00F6F7F8' }
  }
};

var evenCellStyle = exports.evenCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center'
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } }
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00F6F7F8' }
  }
};
var oddCellStyle = exports.oddCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center'
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } }
  }
};

var warningCellStyle = exports.warningCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center'
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } }
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00FFFF00' }
  }
};

var dangerCellStyle = exports.dangerCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center'
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } }
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00EF5350' }
  }
};

var goodCellStyle = exports.goodCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center'
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } }
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '0000C853' }
  }
};

var greatCellStyle = exports.greatCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center'
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } }
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '0029B6F6' }
  }
};