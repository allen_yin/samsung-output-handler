'use strict';

var _dotenv = require('dotenv');

var _dotenv2 = _interopRequireDefault(_dotenv);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ENV_FILE_MAP = {
  dev: '.dev.env',
  local: '.local.env'
};

var APP_ENV = process.env.APP_ENV || 'local';
var ENV_FILE = ENV_FILE_MAP[APP_ENV] || ENV_FILE_MAP['local'];

_dotenv2.default.config({
  path: __dirname + '/../../.env/' + ENV_FILE
});

console.log('APP_ENV: ' + process.env.APP_ENV);
console.log('NODE_ENV: ' + process.env.NODE_ENV);
console.log('PROXY: ' + process.env.PROXY);
console.log('DEBUG: ' + process.env.DEBUG);