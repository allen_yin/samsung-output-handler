'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

require('./setup');

var _Amazon = require('./Amazon');

var _Amazon2 = _interopRequireDefault(_Amazon);

var _Bestbuy = require('./Bestbuy');

var _Bestbuy2 = _interopRequireDefault(_Bestbuy);

var _Walmart = require('./Walmart');

var _Walmart2 = _interopRequireDefault(_Walmart);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  Amazon: _Amazon2.default,
  Bestbuy: _Bestbuy2.default,
  Walmart: _Walmart2.default
};