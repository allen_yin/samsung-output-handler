'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _certificationConfig = require('./certificationConfig');

var _certificationConfig2 = _interopRequireDefault(_certificationConfig);

var _dimensionsWeightConfig = require('./dimensionsWeightConfig');

var _dimensionsWeightConfig2 = _interopRequireDefault(_dimensionsWeightConfig);

var _displayConfig = require('./displayConfig');

var _displayConfig2 = _interopRequireDefault(_displayConfig);

var _expansionConfig = require('./expansionConfig');

var _expansionConfig2 = _interopRequireDefault(_expansionConfig);

var _graphicsConfig = require('./graphicsConfig');

var _graphicsConfig2 = _interopRequireDefault(_graphicsConfig);

var _includedInBoxConfig = require('./includedInBoxConfig');

var _includedInBoxConfig2 = _interopRequireDefault(_includedInBoxConfig);

var _inputDeviceConfig = require('./inputDeviceConfig');

var _inputDeviceConfig2 = _interopRequireDefault(_inputDeviceConfig);

var _memoryConfig = require('./memoryConfig');

var _memoryConfig2 = _interopRequireDefault(_memoryConfig);

var _powerConfig = require('./powerConfig');

var _powerConfig2 = _interopRequireDefault(_powerConfig);

var _processorConfig = require('./processorConfig');

var _processorConfig2 = _interopRequireDefault(_processorConfig);

var _securitySafetyConfig = require('./securitySafetyConfig');

var _securitySafetyConfig2 = _interopRequireDefault(_securitySafetyConfig);

var _soundCameraConfig = require('./soundCameraConfig');

var _soundCameraConfig2 = _interopRequireDefault(_soundCameraConfig);

var _storageConfig = require('./storageConfig');

var _storageConfig2 = _interopRequireDefault(_storageConfig);

var _wirelessConfig = require('./wirelessConfig');

var _wirelessConfig2 = _interopRequireDefault(_wirelessConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = [{
  key: 'productTitle',
  name: 'Product Title'
}, {
  key: 'productDescription',
  name: 'Product Description'
}, {
  key: 'productStatus',
  name: 'Product Status'
}, {
  key: 'launchDate',
  name: 'Launch Date'
}, {
  key: 'materialCode',
  name: 'Material Code'
}, {
  key: 'modelCode',
  name: 'Model Code'
}, {
  key: 'projectName',
  name: 'Project Name'
}, {
  key: 'channelAssortment',
  name: 'Channel Assortment',
  default: 'AMAZON'
}, {
  key: 'ourPrice',
  name: 'Our Price'
}, {
  key: 'listPrice',
  name: 'List Price'
}, {
  key: 'color',
  name: 'Color'
}, {
  key: 'operatingSystem',
  name: 'Operating System'
}, {
  key: 'display',
  name: 'Display',
  children: _displayConfig2.default
}, {
  key: 'processor',
  name: 'Processor',
  children: _processorConfig2.default
}, {
  key: 'storage',
  name: 'Storage',
  children: _storageConfig2.default
}, {
  key: 'memory',
  name: 'Memory',
  children: _memoryConfig2.default
}, {
  key: 'graphics',
  name: 'Graphics',
  children: _graphicsConfig2.default
}, {
  key: 'opticalDiskDrive',
  name: 'Optical Disk Drive'
}, {
  key: 'soundCamera',
  name: 'Sound & Camera',
  children: _soundCameraConfig2.default
}, {
  key: 'wireless',
  name: 'Wireless',
  children: _wirelessConfig2.default
}, {
  key: 'expansion',
  name: 'Expansion',
  children: _expansionConfig2.default
}, {
  key: 'inputDevice',
  name: 'Input Device',
  children: _inputDeviceConfig2.default
}, {
  key: 'countryOfOrigin',
  name: 'Country Of Origin'
}, {
  key: 'power',
  name: 'Power',
  children: _powerConfig2.default
}, {
  key: 'securitySafety',
  name: 'Security & Safety',
  children: _securitySafetyConfig2.default
}, {
  key: 'certification',
  name: 'Certification',
  children: _certificationConfig2.default
}, {
  key: 'includedInBox',
  name: 'Included In Box',
  children: _includedInBoxConfig2.default
}, {
  key: 'material',
  name: 'Material'
}, {
  key: 'upc',
  name: 'UPC'
}, {
  key: 'dimensionsWeight',
  name: 'Dimensions & Weight',
  children: _dimensionsWeightConfig2.default
}, {
  key: 'warranty',
  name: 'Warranty'
}];