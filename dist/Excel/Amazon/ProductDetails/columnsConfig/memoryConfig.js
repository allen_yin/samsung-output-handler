'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = [{
  key: 'systemMemory',
  name: 'System Memory'
}, {
  key: 'memoryConfiguration',
  name: 'Memory Configuration',
  width: 22
}, {
  key: 'memoryType',
  name: 'Memory Type'
}, {
  key: 'maxSystemMemory',
  name: 'Max. System Memory',
  width: 22
}, {
  key: 'numberOfSlots',
  name: 'Number Of Slots'
}];