'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = [{
  key: 'backlitKeyboard',
  name: 'Backlit Keyboard'
}, {
  key: 'numericKeypad',
  name: 'Numeric Keypad'
}, {
  key: 'spillResistant',
  name: 'Spill Resistant'
}, {
  key: 'numberOfKeys',
  name: 'Number Of Keys'
}];