'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = [{
  key: 'epeatRating',
  name: 'Epeat Rating'
}, {
  key: 'energyStarRating',
  name: 'Energy Star Rating'
}];