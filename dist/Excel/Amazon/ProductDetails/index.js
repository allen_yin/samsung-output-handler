'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _exportExcelHelper = require('../../../helpers/exportExcelHelper');

var _exportExcelHelper2 = _interopRequireDefault(_exportExcelHelper);

var _columnsConfig = require('./columnsConfig');

var _columnsConfig2 = _interopRequireDefault(_columnsConfig);

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

var _defaultConfig = require('./columnsConfig/defaultConfig');

var _defaultConfig2 = _interopRequireDefault(_defaultConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var LOG = (0, _debug2.default)('output-handler-excel-amazon');

var rawToExcelArrayRawJS = function rawToExcelArrayRawJS(_ref) {
  var _ref$productInfo = _ref.productInfo,
      productInfo = _ref$productInfo === undefined ? {} : _ref$productInfo;

  var excelArray = [];
  var autoFilter = undefined;
  var columns = [];
  var mergeCells = {};
  var styles = {};
  var xFixed = 6;
  var yFixed = 0;
  var heights = [];

  var subHeaderRow = {};
  var dataRow = {};

  heights = [{ row: 1, height: 30 }, { row: 2, height: 30 }, { row: 3, height: 50 }];

  _columnsConfig2.default.forEach(function (columnConfig) {
    if (!columnConfig.children) {
      //setup value
      if (productInfo[columnConfig.key]) {
        dataRow[columnConfig.key] = productInfo[columnConfig.key];
      } else if (columnConfig.default) {
        dataRow[columnConfig.key] = columnConfig.default;
      }

      //setup merge
      mergeCells[_exportExcelHelper2.default.getExcelColumnName(columns.length + 1) + '1:' + _exportExcelHelper2.default.getExcelColumnName(columns.length + 1) + '2'] = {};

      //setup header
      columns.push({
        header: columnConfig.name,
        key: columnConfig.key,
        width: columnConfig.width || _defaultConfig2.default.width
      });

      //setup child header
    } else {
      mergeCells[_exportExcelHelper2.default.getExcelColumnName(columns.length + 1) + '1:' + _exportExcelHelper2.default.getExcelColumnName(columns.length + columnConfig.children.length) + '1'] = {};
      columnConfig.children.forEach(function (child, index) {
        if (productInfo[columnConfig.key] && productInfo[columnConfig.key][child.key]) {
          dataRow[columnConfig.key + ':' + child.key] = productInfo[columnConfig.key][child.key];
        } else if (child.default) {
          dataRow[columnConfig.key + ':' + child.key] = child.default;
        }
        columns.push({
          header: index === 0 ? columnConfig.name : '',
          key: columnConfig.key + ':' + child.key,
          width: child.width || _defaultConfig2.default.width
        });
        subHeaderRow[columnConfig.key + ':' + child.key] = child.name;
      });
    }
  });

  excelArray.push(subHeaderRow);
  excelArray.push(dataRow);

  //setup styles
  columns.forEach(function (column, index) {
    styles[_exportExcelHelper2.default.getExcelColumnName(index + 1) + '1'] = _exportExcelHelper.styles.subHeaderStyle;
    styles[_exportExcelHelper2.default.getExcelColumnName(index + 1) + '2'] = _exportExcelHelper.styles.subHeaderStyle;
    styles[_exportExcelHelper2.default.getExcelColumnName(index + 1) + '3'] = _exportExcelHelper.styles.oddCellStyle;
  });

  return {
    autoFilter: autoFilter,
    columns: columns,
    excelArray: excelArray,
    heights: heights,
    mergeCells: mergeCells,
    styles: styles,
    xFixed: xFixed,
    yFixed: yFixed
  };
};

exports.default = function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var _ref3 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        productInfo = _ref3.productInfo;

    var sheets, arr, workbook;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            sheets = {};

            LOG('start generating excel array.....');
            arr = rawToExcelArrayRawJS({
              productInfo: productInfo
            });

            sheets['Spec File'] = {
              autoFilter: arr.autoFilter,
              data: arr.excelArray,
              columns: arr.columns,
              heights: arr.heights,
              styles: arr.styles,
              mergeCells: arr.mergeCells,
              views: [{ state: 'frozen', xSplit: arr.xFixed, ySplit: arr.yFixed }]
            };
            LOG('start generating workbook.....');
            _context.next = 7;
            return _exportExcelHelper2.default.prepareExcelWorkbookFromArray({
              workbooks: sheets
            });

          case 7:
            workbook = _context.sent;

            LOG('workbook generated.....');
            LOG(workbook);
            return _context.abrupt('return', workbook);

          case 11:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined);
  }));

  return function () {
    return _ref2.apply(this, arguments);
  };
}();