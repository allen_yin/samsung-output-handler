'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ProductDetails = require('./ProductDetails');

var _ProductDetails2 = _interopRequireDefault(_ProductDetails);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  ProductDetails: _ProductDetails2.default
};