'use strict';

require('../setup/server.js');

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _bbuUserJsonTpl = require('./bbuUserJsonTpl');

var _bbuUserJsonTpl2 = _interopRequireDefault(_bbuUserJsonTpl);

var _index = require('../index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var testData = {
  modelCode: 'B000SX4C7Q',
  productDescription: 'Samsung Genuine Refrigerator Water Filters give your family clean, fresh water, while protecting your refrigerator and home from potential damage. This filter uses a powerful concentrated carbon filter to eliminate over 99 percent of potentially harmful contaminants. Samsung Genuine filters are produced in both Korea and Mexico. Shipping Note: Shipping to PO Boxes and APO addresses is not available for this item.',
  productTitle: 'Samsung Genuine DA29-00003G Refrigerator Water Filter, 1 Pack',
  launchDate: 'May 22, 2004',
  dimensionsWeight: {
    unitPackageWeight: '11.5 ounces'
  },
  ourPrice: '$37.90',
  listPrice: null,
  features: [['Fast print speeds up to 15 ISO ppm* in black', 'Creates the 1st page out in as little as 8.5 seconds* for rapid document production.------test-----\n--------'], ['Be sure you have the right cartridge', 'Enter your printer&apos;s model number into our Ink and Toner Finder to find compatible cartridges. <a href="/site/printers/printer-ink-toner/abcat0807000.c?id=abcat0807000" target="_blank">Get started &#x203A;</a>'], ['Built-in wireless LAN (802.11b/g/n)', 'Ensures fast data transfer from a wireless connection.'], ['400MHz processor and 64MB memory', 'Offer improved performance when printing complex documents.']],
  specifications: [{
    keys: 'Key Specs',
    values: [['ENERGY STAR Certified', 'Yes'], ['Printer Type', 'Printer'], ['Duty Cycle', 'Up to 10,000'], ['ISO Mono Print Speed', '15 pages per minute'], ['Monochromatic/Color', 'Monochromatic']]
  }, {
    keys: 'Connectivity',
    values: [['Printer Connectivity', 'Wi-Fi, USB 2.0, Google Cloud Print'], ['Minimum System Requirements', 'PC: Windows 2003, 2008, 2008 R2, Server 2012, XP, Vista, 7 or 8 (32- or 64-bit); Mac: OS X 10.5 - 10.9; Linux']]
  }]
};
var walmartTest = {

  sku: "442B9F0LA4SZ",
  productId: "442B9F0LA4SZ",
  shortDescription: "Introducing The Frame from Samsung - a revolutionary way to think about your TV. When you're not watching the brilliant 4K UHD TV with HDR, it turns into a beautiful work of art on your wall. With an array of paintings, prints, photos, and frames to suit every mood and style, The Frame TV goes beyond entertainment and becomes an expression of you",
  manufacturerPartNumber: "UN55LS003AFXZA"
};

var LOG = (0, _debug2.default)('output-handler-main');
var fileName = 'test-amazon-pdetail.xlsx';
var main = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var jsonData, xml, walmartXML, workbook;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            LOG('testing .............');
            _context.prev = 1;
            jsonData = _bbuUserJsonTpl2.default;
            _context.next = 5;
            return _index2.default.Bestbuy.ProductDetail.Xml(jsonData);

          case 5:
            xml = _context.sent;

            _fs2.default.writeFile('storage/xml/besyBuyXML.xml', xml, function (err, docs) {
              LOG('bestbuy write is done.....');
            });

            _context.next = 9;
            return _index2.default.Walmart.ProductDetail.Xml(walmartTest);

          case 9:
            walmartXML = _context.sent;

            _fs2.default.writeFile('storage/xml/walmartXML.xml', walmartXML, function (err, docs) {
              LOG('walmart write is done.....');
            });

            _context.next = 13;
            return _index2.default.Amazon.ProductDetail.Excel({
              productInfo: testData
            });

          case 13:
            workbook = _context.sent;

            workbook.xlsx.writeFile('storage/xlsx/' + fileName).then(function () {
              LOG('write file is done.....');
            });
            _context.next = 20;
            break;

          case 17:
            _context.prev = 17;
            _context.t0 = _context['catch'](1);

            LOG(_context.t0);

          case 20:

            LOG(res);

          case 21:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[1, 17]]);
  }));

  return function main() {
    return _ref.apply(this, arguments);
  };
}();

LOG('start main....');

main();