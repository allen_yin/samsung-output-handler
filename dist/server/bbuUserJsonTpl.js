'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  version: '1.1',
  encoding: 'GBK',
  standalone: 'true',
  language: 'en',
  market: 'us',
  partNumber: 'XE501C13-K02US',
  shortDescription: 'shortDescriptionshortDescriptionshortDescription',
  upcEan: '887276269436',
  launchDate: '07-01-2018',
  endOfLifeDate: '',
  marketingText: '',
  largeImage: 'https://images.google.com',
  energyLabelImages: [],
  images: [{
    image: 'https://images.google.com'
  }, {
    image: 'https://images.google.com'
  }, {
    image: 'https://images.google.com'
  }],
  logoItems: {
    item: [{
      image: 'https://images.google.com'
    }, {
      image: 'https://images.google.com'
    }, {
      image: 'https://images.google.com'
    }, {
      image: 'https://images.google.com'
    }]
  },
  keySellingPoints: [{
    item: 'a'
  }, {
    item: 'a'
  }, {
    item: 'a'
  }, {
    item: 'a'
  }],
  features: [['Fast print speeds up to 15 ISO ppm* in black', 'Creates the 1st page out in as little as 8.5 seconds* for rapid document production.------test-----\n--------'], ['Be sure you have the right cartridge', 'Enter your printer&apos;s model number into our Ink and Toner Finder to find compatible cartridges. <a href="/site/printers/printer-ink-toner/abcat0807000.c?id=abcat0807000" target="_blank">Get started &#x203A;</a>'], ['Built-in wireless LAN (802.11b/g/n)', 'Ensures fast data transfer from a wireless connection.'], ['400MHz processor and 64MB memory', 'Offer improved performance when printing complex documents.']],
  specifications: [{
    keys: 'Key Specs',
    values: [['ENERGY STAR Certified', 'Yes'], ['Printer Type', 'Printer'], ['Duty Cycle', 'Up to 10,000'], ['ISO Mono Print Speed', '15 pages per minute'], ['Monochromatic/Color', 'Monochromatic']]
  }, {
    keys: 'Connectivity',
    values: [['Printer Connectivity', 'Wi-Fi, USB 2.0, Google Cloud Print'], ['Minimum System Requirements', 'PC: Windows 2003, 2008, 2008 R2, Server 2012, XP, Vista, 7 or 8 (32- or 64-bit); Mac: OS X 10.5 - 10.9; Linux']]
  }],
  attributeGroup: [{
    name: 'testAttributeGroup',
    attribute: {
      name: 'testAttribute',
      value: 'testAttributeValue'
    }
  }, {
    name: 'testAttributeGroup',
    attribute: {
      name: 'testAttribute',
      value: 'testAttributeValue'
    }
  }, {
    name: 'testAttributeGroup',
    attribute: {
      name: 'testAttribute',
      value: 'testAttributeValue'
    }
  }, {
    name: 'testAttributeGroup',
    attribute: {
      name: 'testAttribute',
      value: 'testAttributeValue'
    }
  }],
  pdfUserManual: 'http://org.downloadcenter.samsung.com/downloadfile/ContentsFile.aspx?CDSite=US&amp;CttFileID=7046424&amp;CDCttType=UM&amp;ModelType=C&amp;ModelName=XE501C13-K01US&amp;VPath=UM/201805/20180525091920106/Chromebook_Manual_ENG.pdf'
};