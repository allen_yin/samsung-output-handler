'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _exportExcelHelper = require('../../../helpers/exportExcelHelper');

var _exportExcelHelper2 = _interopRequireDefault(_exportExcelHelper);

var _columnsConfig = require('./columnsConfig');

var _columnsConfig2 = _interopRequireDefault(_columnsConfig);

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

var _defaultConfig = require('./columnsConfig/defaultConfig');

var _defaultConfig2 = _interopRequireDefault(_defaultConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LOG = (0, _debug2.default)('samsung-output-excel-amazon-rawToExcelArray');

exports.default = function (_ref) {
  var _ref$productInfo = _ref.productInfo,
      productInfo = _ref$productInfo === undefined ? {} : _ref$productInfo;

  var excelArray = [];
  var autoFilter = undefined;
  var columns = [];
  var mergeCells = {};
  var styles = {};
  var xFixed = 6;
  var yFixed = 0;
  var heights = [];

  var subHeaderRow = {};
  var dataRow = {};

  heights = [{ row: 1, height: 30 }, { row: 2, height: 30 }, { row: 3, height: 50 }];

  (0, _columnsConfig2.default)({
    features: productInfo.features,
    specifications: productInfo.specifications
  }).forEach(function (columnConfig) {
    if (!columnConfig.children) {
      //setup value
      if (productInfo[columnConfig.key]) {
        dataRow[columnConfig.key] = productInfo[columnConfig.key];
      } else if (columnConfig.default) {
        dataRow[columnConfig.key] = columnConfig.default;
      }

      //setup merge
      mergeCells[_exportExcelHelper2.default.getExcelColumnName(columns.length + 1) + '1:' + _exportExcelHelper2.default.getExcelColumnName(columns.length + 1) + '2'] = {};

      //setup header
      columns.push({
        header: columnConfig.name,
        key: columnConfig.key,
        width: columnConfig.width || _defaultConfig2.default.width
      });

      //setup child header
    } else {
      mergeCells[_exportExcelHelper2.default.getExcelColumnName(columns.length + 1) + '1:' + _exportExcelHelper2.default.getExcelColumnName(columns.length + columnConfig.children.length) + '1'] = {};
      columnConfig.children.forEach(function (child, index) {
        if (productInfo[columnConfig.key] && productInfo[columnConfig.key][child.key]) {
          dataRow[columnConfig.key + ':' + child.key] = productInfo[columnConfig.key][child.key];
        } else if (child.default) {
          dataRow[columnConfig.key + ':' + child.key] = child.default;
        }
        columns.push({
          header: index === 0 ? columnConfig.name : '',
          key: columnConfig.key + ':' + child.key,
          width: child.width || _defaultConfig2.default.width
        });
        subHeaderRow[columnConfig.key + ':' + child.key] = child.name;
      });
    }
  });

  if (productInfo.features) {
    productInfo.features.forEach(function (feature) {
      dataRow['features:' + feature[0]] = feature[1];
    });
  }
  if (productInfo.specifications) {
    productInfo.specifications.forEach(function (specification) {
      specification.values && specification.values.forEach(function (value) {
        dataRow['specifications:' + value[0]] = value[1];
      });
    });
  }

  excelArray.push(subHeaderRow);
  excelArray.push(dataRow);

  //setup styles
  columns.forEach(function (column, index) {
    styles[_exportExcelHelper2.default.getExcelColumnName(index + 1) + '1'] = _exportExcelHelper.styles.subHeaderStyle;
    styles[_exportExcelHelper2.default.getExcelColumnName(index + 1) + '2'] = _exportExcelHelper.styles.subHeaderStyle;
    styles[_exportExcelHelper2.default.getExcelColumnName(index + 1) + '3'] = _exportExcelHelper.styles.oddCellStyle;
  });

  return {
    autoFilter: autoFilter,
    columns: columns,
    excelArray: excelArray,
    heights: heights,
    mergeCells: mergeCells,
    styles: styles,
    xFixed: xFixed,
    yFixed: yFixed
  };
};