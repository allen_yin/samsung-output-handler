'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

var _exportExcelHelper = require('../../../helpers/exportExcelHelper');

var _exportExcelHelper2 = _interopRequireDefault(_exportExcelHelper);

var _rawToExcelArrayRawJS = require('./rawToExcelArrayRawJS');

var _rawToExcelArrayRawJS2 = _interopRequireDefault(_rawToExcelArrayRawJS);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var LOG = (0, _debug2.default)('output-handler-excel-amazon');

exports.default = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        productInfo = _ref2.productInfo;

    var sheets, arr, workbook;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            sheets = {};

            LOG('start generating excel array.....');
            arr = (0, _rawToExcelArrayRawJS2.default)({
              productInfo: productInfo
            });

            sheets['Spec File'] = {
              autoFilter: arr.autoFilter,
              data: arr.excelArray,
              columns: arr.columns,
              heights: arr.heights,
              styles: arr.styles,
              mergeCells: arr.mergeCells,
              views: [{ state: 'frozen', xSplit: arr.xFixed, ySplit: arr.yFixed }]
            };
            LOG('start generating workbook.....');
            _context.next = 7;
            return _exportExcelHelper2.default.prepareExcelWorkbookFromArray({
              workbooks: sheets
            });

          case 7:
            workbook = _context.sent;

            LOG('workbook generated.....');
            LOG(workbook);
            return _context.abrupt('return', workbook);

          case 11:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined);
  }));

  return function () {
    return _ref.apply(this, arguments);
  };
}();