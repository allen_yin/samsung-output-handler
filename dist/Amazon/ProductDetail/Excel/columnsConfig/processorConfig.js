'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = [{
  key: 'processor',
  name: 'Processor'
}, {
  key: 'processorSpeed',
  name: 'Processor Speed (GHz)',
  width: 22
}, {
  key: 'turboBoost',
  name: 'Turbo Boost (GHz)',
  width: 22
}, {
  key: 'cpuCache',
  name: 'CPU Cache'
}];