'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = [{
  key: 'productDimensions',
  name: 'Product Dimensions (LxWxH, inch)',
  width: 30
}, {
  key: 'productWeight',
  name: 'Product Weight (w/Std. Battery, lbs)',
  width: 30
}, {
  key: 'packageDimensions',
  name: 'Package Dimensions (LxWxH, inch)',
  width: 30
}, {
  key: 'unitPackageWeight',
  name: 'Unit + Package Weight (lbs)',
  width: 30
}];