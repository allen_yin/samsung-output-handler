'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = [{
  key: 'vga',
  name: 'VGA'
}, {
  key: 'hdmi',
  name: 'HDMI'
}, {
  key: 'headphoneOut',
  name: 'Headphone Out'
}, {
  key: 'microphoneIn',
  name: 'Microphone In'
}, {
  key: 'usbPorts',
  name: 'USB Ports (Total)'
}, {
  key: 'sleepAndChargeUSB',
  name: 'Sleep-and-Charge USB',
  width: 22
}, {
  key: 'multiCardSlot',
  name: 'Multi Card Slot'
}, {
  key: 'rj45',
  name: 'RJ45 (LAN)'
}, {
  key: 'dockingPort',
  name: 'Docking Port'
}];