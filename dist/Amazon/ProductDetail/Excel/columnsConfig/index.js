'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _certificationConfig = require('./certificationConfig');

var _certificationConfig2 = _interopRequireDefault(_certificationConfig);

var _dimensionsWeightConfig = require('./dimensionsWeightConfig');

var _dimensionsWeightConfig2 = _interopRequireDefault(_dimensionsWeightConfig);

var _displayConfig = require('./displayConfig');

var _displayConfig2 = _interopRequireDefault(_displayConfig);

var _expansionConfig = require('./expansionConfig');

var _expansionConfig2 = _interopRequireDefault(_expansionConfig);

var _graphicsConfig = require('./graphicsConfig');

var _graphicsConfig2 = _interopRequireDefault(_graphicsConfig);

var _includedInBoxConfig = require('./includedInBoxConfig');

var _includedInBoxConfig2 = _interopRequireDefault(_includedInBoxConfig);

var _inputDeviceConfig = require('./inputDeviceConfig');

var _inputDeviceConfig2 = _interopRequireDefault(_inputDeviceConfig);

var _memoryConfig = require('./memoryConfig');

var _memoryConfig2 = _interopRequireDefault(_memoryConfig);

var _powerConfig = require('./powerConfig');

var _powerConfig2 = _interopRequireDefault(_powerConfig);

var _processorConfig = require('./processorConfig');

var _processorConfig2 = _interopRequireDefault(_processorConfig);

var _securitySafetyConfig = require('./securitySafetyConfig');

var _securitySafetyConfig2 = _interopRequireDefault(_securitySafetyConfig);

var _soundCameraConfig = require('./soundCameraConfig');

var _soundCameraConfig2 = _interopRequireDefault(_soundCameraConfig);

var _storageConfig = require('./storageConfig');

var _storageConfig2 = _interopRequireDefault(_storageConfig);

var _wirelessConfig = require('./wirelessConfig');

var _wirelessConfig2 = _interopRequireDefault(_wirelessConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _getFeatureSubtitle = function _getFeatureSubtitle(features) {
  var children = [];
  for (var i = 0; i < features.length; i++) {
    children.push({
      key: features[i][0],
      name: features[i][0]
    });
  }
  return {
    key: 'features',
    name: 'Features',
    children: children
  };
};

var _getSpecSubtitle = function _getSpecSubtitle(specifications) {
  var children = [];
  for (var i = 0; i < specifications.length; i++) {
    specifications[i].values && specifications[i].values.forEach(function (value) {
      children.push({
        key: value[0],
        name: value[0]
      });
    });
  }
  return {
    key: 'specifications',
    name: 'Specifications',
    children: children
  };
};

exports.default = function (_ref) {
  var features = _ref.features,
      specifications = _ref.specifications;

  var res = [{
    key: 'productTitle',
    name: 'Product Title'
  }, {
    key: 'productDescription',
    name: 'Product Description'
  }, {
    key: 'productStatus',
    name: 'Product Status'
  }, {
    key: 'launchDate',
    name: 'Launch Date'
  }, {
    key: 'materialCode',
    name: 'Material Code'
  }, {
    key: 'modelCode',
    name: 'Model Code'
  }, {
    key: 'projectName',
    name: 'Project Name'
  }, {
    key: 'channelAssortment',
    name: 'Channel Assortment',
    default: 'AMAZON'
  }, {
    key: 'ourPrice',
    name: 'Our Price'
  }, {
    key: 'listPrice',
    name: 'List Price'
  }, {
    key: 'color',
    name: 'Color'
  }, {
    key: 'operatingSystem',
    name: 'Operating System'
  },
  // {
  //   key: 'display',
  //   name: 'Display',
  //   children: displayConfig,
  // },
  // {
  //   key: 'processor',
  //   name: 'Processor',
  //   children: processorConfig,
  // },
  // {
  //   key: 'storage',
  //   name: 'Storage',
  //   children: storageConfig,
  // },
  // {
  //   key: 'memory',
  //   name: 'Memory',
  //   children: memoryConfig,
  // },
  // {
  //   key: 'graphics',
  //   name: 'Graphics',
  //   children: graphicsConfig,
  // },
  {
    key: 'opticalDiskDrive',
    name: 'Optical Disk Drive'
  },
  // {
  //   key: 'soundCamera',
  //   name: 'Sound & Camera',
  //   children: soundCameraConfig,
  // },
  // {
  //   key: 'wireless',
  //   name: 'Wireless',
  //   children: wirelessConfig,
  // },
  // {
  //   key: 'expansion',
  //   name: 'Expansion',
  //   children: expansionConfig,
  // },
  // {
  //   key: 'inputDevice',
  //   name: 'Input Device',
  //   children: inputDeviceConfig,
  // },
  {
    key: 'countryOfOrigin',
    name: 'Country Of Origin'
  },
  // {
  //   key: 'power',
  //   name: 'Power',
  //   children: powerConfig,
  // },
  // {
  //   key: 'securitySafety',
  //   name: 'Security & Safety',
  //   children: securitySafetyConfig,
  // },
  // {
  //   key: 'certification',
  //   name: 'Certification',
  //   children: certificationConfig,
  // },
  // {
  //   key: 'includedInBox',
  //   name: 'Included In Box',
  //   children: includedInBoxConfig,
  // },
  {
    key: 'material',
    name: 'Material'
  }, {
    key: 'upc',
    name: 'UPC'
  },
  // {
  //   key: 'dimensionsWeight',
  //   name: 'Dimensions & Weight',
  //   children: dimensionsWeightConfig,
  // },
  {
    key: 'warranty',
    name: 'Warranty'
  }];
  // if (features && features.length !== 0) {
  //   res.push(_getFeatureSubtitle(features))
  // }
  if (specifications && specifications.length !== 0) {
    //res.push(_getSpecSubtitle(specifications))
    for (var i = 0; i < specifications.length; i++) {
      specifications[i].values && specifications[i].values.forEach(function (value) {
        res.push({
          key: "specifications:" + value[0],
          name: value[0]
        });
      });
    }
  }
  return res;
};