'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = [{
  key: 'wirelessLan',
  name: 'Wireless LAN'
}, {
  key: 'nameOfWirelessLANCard',
  name: 'Name of Wireless LAN Card',
  width: 22
}, {
  key: 'wirelessLanAntenna',
  name: 'Wireless LAN Antenna',
  width: 22
}, {
  key: 'bluetooth',
  name: 'Bluetooth'
}, {
  key: 'widi',
  name: 'WiDi'
}];