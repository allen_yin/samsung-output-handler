'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = [{
  key: 'storageCapacity',
  name: 'Storage Capacity'
}, {
  key: 'storageTechnology',
  name: 'Storage Technology'
}, {
  key: 'storageInterface',
  name: 'Storage Interface'
}];