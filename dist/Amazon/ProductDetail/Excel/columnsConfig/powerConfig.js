'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = [{
  key: 'acAdapter',
  name: 'AC Adapter'
}, {
  key: 'numOfCell',
  name: '# of Cell /'
}, {
  key: 'cellType',
  name: 'Cell Type'
}, {
  key: 'mAh',
  name: 'mAh'
}, {
  key: 'wattHours',
  name: 'Watt Hours'
}, {
  key: 'batteryLife',
  name: 'Battery Life'
}, {
  key: 'testUsed',
  name: 'Test Used'
}, {
  key: 'wercs',
  name: 'WERCS'
}, {
  key: 'wpsId',
  name: 'WPS ID'
}, {
  key: 'batteryNum',
  name: 'Battery #'
}];