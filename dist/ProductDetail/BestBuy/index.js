'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Xml = require('./Xml');

var _Xml2 = _interopRequireDefault(_Xml);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  Xml: _Xml2.default
};