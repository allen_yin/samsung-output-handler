"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _debug = require("debug");

var _debug2 = _interopRequireDefault(_debug);

var _xmlJs = require("xml-js");

var _xmlJs2 = _interopRequireDefault(_xmlJs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LOG = (0, _debug2.default)("output-handler-helpers");

var nestElement = function nestElement(arr) {
  var res = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var field = arguments[2];

  console.log(typeof arr === "undefined" ? "undefined" : _typeof(arr), arr, arr.length, field);
  for (var i = 0; i < arr.length; i++) {
    Object.keys(arr[i]).forEach(function (key) {
      if (_typeof(arr[i]) !== "object") {
        res.push("" + arr[i][key]);
      } else {
        res.push(nestObjElement(arr[i], res = [], field));
      }
    });
  }
  return res;
};

var nestArrObj = function nestArrObj(arr) {
  var res = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var field = arguments[2];

  console.log(typeof arr === "undefined" ? "undefined" : _typeof(arr), arr, arr.length, field);
  for (var i = 0; i < arr.length; i++) {
    Object.keys(arr[i]).forEach(function (key) {
      res.push("" + arr[i][key]);
    });
  }
  return res;
};

var nestObjElement = function nestObjElement(obj) {
  var res = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var field = arguments[2];

  Object.keys(obj).forEach(function (key) {
    res.push(key + ":" + obj[key]);
  });
  return res;
};

var emptyObj = "";

exports.default = function (jsonData) {
  var structuredJson = {
    "_declaration": {
      "_attributes": {
        "version": jsonData.version || "1.0",
        "encoding": jsonData.encoding || "UTF-8",
        "standalone": jsonData.standalone || "yes"
      }
    },
    "Feed": {
      "_attributes": {
        "language": jsonData.language || "en",
        "market": jsonData.market || "us"
      },
      "Product": {
        "PartNumber": jsonData.partNumber,
        "ProductDescription": jsonData.shortDescription,
        "UpcEan": jsonData.upcEan,
        "LaunchDate": jsonData.launchDate,
        "EndOfLifeDate": jsonData.endOfLifeDate,
        "HeroImage": jsonData.largeImage,
        "Images": {
          "Image": nestArrObj(jsonData.images, [], 1)
        },
        "EnergyLabelImages": jsonData.energyLabelImages || emptyObj,
        "LogoItems": jsonData.logoItems,
        "KeySellingPoints": {
          "item": nestArrObj(jsonData.keySellingPoints, [], 2)
        },
        "ProductFeatures": jsonData.productFeatures,
        "AttributeGroup": jsonData.attributeGroup,
        "PdfProductDataSheet": jsonData.pdfProductDataSheet,
        "PdfUserManual": jsonData.pdfUserManual,
        "PdfQuickStartGuide": jsonData.pdfQuickStartGuide,
        "WarrantyCard": jsonData.warrantyCard,
        "InstallationGuide": jsonData.installationGuide,
        "PdfFTCEnergyGuide": jsonData.pdfFTCEnergyGuide,
        "PdfFTCLightingFacts": jsonData.pdfFTCLightingFacts,
        "ProductBrochures": jsonData.productBrochures,
        "PdfMsds": jsonData.pdfMsds,
        "PdfEUEnergyLabel": jsonData.pdfEUEnergyLabel,
        "AccessoriesPartNumbers": jsonData.accessoriesPartNumbers,
        "MainProductsPartNumbers": jsonData.mainProductsPartNumbers,
        "Videos": jsonData.videos,
        "Footnotes": jsonData.footnotes,
        "LegalCopy": jsonData.legalCopy

      }
    }
  };
  console.log(jsonData.productFeatures);
  var options = { compact: true, ignoreComment: true, spaces: 4 };
  var resultXml = _xmlJs2.default.json2xml(structuredJson, options);
  return resultXml;
};