'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Walmart = require('./Walmart');

var _Walmart2 = _interopRequireDefault(_Walmart);

var _BestBuy = require('./BestBuy');

var _BestBuy2 = _interopRequireDefault(_BestBuy);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  Walmart: _Walmart2.default,
  BestBuy: _BestBuy2.default
};