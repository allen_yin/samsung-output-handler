'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

var _exportExcelHelper = require('../../../helpers/exportExcelHelper');

var _exportExcelHelper2 = _interopRequireDefault(_exportExcelHelper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var LOG = (0, _debug2.default)('output-handler-walmart-excel');

function rawToExcelArrayRawJS(params) {
  var excelArray = [[]];
  var autoFilter = undefined;
  var columnsConfig = [];
  var mergeCells = undefined;
  var styles = {};
  var xFixed = 0;
  var yFixed = 0;
  var heights = [];

  return {
    autoFilter: autoFilter,
    columnsConfig: columnsConfig,
    excelArray: excelArray,
    heights: heights,
    mergeCells: mergeCells,
    styles: styles,
    xFixed: xFixed,
    yFixed: yFixed
  };
}

exports.default = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        productInfo = _ref2.productInfo;

    var sheets, arr, workbook, fileName;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            sheets = {};

            LOG('start generating excel array.....');
            arr = rawToExcelArrayRawJS({
              productInfo: productInfo
            });

            sheets['testSheet'] = {
              autoFilter: arr.autoFilter,
              data: arr.excelArray,
              columns: arr.columnsConfig,
              heights: arr.heights,
              styles: arr.styles,
              mergeCells: arr.mergeCells
            };
            LOG('start generating workbook.....');
            _context.next = 7;
            return _exportExcelHelper2.default.prepareExcelWorkbookFromArray({
              workbooks: sheets
            });

          case 7:
            workbook = _context.sent;
            fileName = 'test-export.xlsx';

            LOG('workbook generated.....');
            LOG(workbook);
            return _context.abrupt('return', workbook);

          case 12:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined);
  }));

  return function () {
    return _ref.apply(this, arguments);
  };
}();