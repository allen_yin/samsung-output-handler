'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Excel = require('./Excel');

var _Excel2 = _interopRequireDefault(_Excel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  Excel: _Excel2.default
};