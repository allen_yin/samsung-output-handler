import exportExcelHelper, {
  styles as excelStyles,
} from '../../../helpers/exportExcelHelper'
import columnsConfig from './columnsConfig'
import debug from 'debug'
import defaultConfig from './columnsConfig/defaultConfig'

const LOG = debug('samsung-output-excel-amazon-rawToExcelArray')

export default ({ productInfo = {} }) => {
  let excelArray = []
  let autoFilter = undefined
  let columns = []
  let mergeCells = {}
  let styles = {}
  let xFixed = 6
  let yFixed = 0
  let heights = []

  let subHeaderRow = {}
  let dataRow = {}

  heights = [
    { row: 1, height: 30 },
    { row: 2, height: 30 },
    { row: 3, height: 50 },
  ]

  columnsConfig({
    features: productInfo.features,
    specifications: productInfo.specifications,
  }).forEach(columnConfig => {
    if (!columnConfig.children) {
      //setup value
      if (productInfo[columnConfig.key]) {
        dataRow[columnConfig.key] = productInfo[columnConfig.key]
      } else if (columnConfig.default) {
        dataRow[columnConfig.key] = columnConfig.default
      }

      //setup merge
      mergeCells[
        exportExcelHelper.getExcelColumnName(columns.length + 1) +
          '1:' +
          exportExcelHelper.getExcelColumnName(columns.length + 1) +
          '2'
      ] = {}

      //setup header
      columns.push({
        header: columnConfig.name,
        key: columnConfig.key,
        width: columnConfig.width || defaultConfig.width,
      })

      //setup child header
    } else {
      mergeCells[
        exportExcelHelper.getExcelColumnName(columns.length + 1) +
          '1:' +
          exportExcelHelper.getExcelColumnName(
            columns.length + columnConfig.children.length
          ) +
          '1'
      ] = {}
      columnConfig.children.forEach((child, index) => {
        if (
          productInfo[columnConfig.key] &&
          productInfo[columnConfig.key][child.key]
        ) {
          dataRow[columnConfig.key + ':' + child.key] =
            productInfo[columnConfig.key][child.key]
        } else if (child.default) {
          dataRow[columnConfig.key + ':' + child.key] = child.default
        }
        columns.push({
          header: index === 0 ? columnConfig.name : '',
          key: columnConfig.key + ':' + child.key,
          width: child.width || defaultConfig.width,
        })
        subHeaderRow[columnConfig.key + ':' + child.key] = child.name
      })
    }
  })

  if (productInfo.features) {
    productInfo.features.forEach(feature => {
      dataRow['features:' + feature[0]] = feature[1]
    })
  }
  if (productInfo.specifications) {
    productInfo.specifications.forEach(specification => {
      specification.values &&
        specification.values.forEach(value => {
          dataRow['specifications:' + value[0]] = value[1]
        })
    })
  }

  excelArray.push(subHeaderRow)
  excelArray.push(dataRow)

  //setup styles
  columns.forEach((column, index) => {
    styles[exportExcelHelper.getExcelColumnName(index + 1) + '1'] =
      excelStyles.subHeaderStyle
    styles[exportExcelHelper.getExcelColumnName(index + 1) + '2'] =
      excelStyles.subHeaderStyle
    styles[exportExcelHelper.getExcelColumnName(index + 1) + '3'] =
      excelStyles.oddCellStyle
  })

  return {
    autoFilter,
    columns,
    excelArray,
    heights,
    mergeCells,
    styles,
    xFixed,
    yFixed,
  }
}
