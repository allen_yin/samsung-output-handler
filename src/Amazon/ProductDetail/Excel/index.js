import debug from 'debug'
import exportExcelHelper from '../../../helpers/exportExcelHelper'
import rawToExcelArrayRawJS from './rawToExcelArrayRawJS'

const LOG = debug('output-handler-excel-amazon')

export default async ({ productInfo } = {}) => {
  let sheets = {}
  LOG('start generating excel array.....')
  const arr = rawToExcelArrayRawJS({
    productInfo,
  })
  sheets['Spec File'] = {
    autoFilter: arr.autoFilter,
    data: arr.excelArray,
    columns: arr.columns,
    heights: arr.heights,
    styles: arr.styles,
    mergeCells: arr.mergeCells,
    views: [{ state: 'frozen', xSplit: arr.xFixed, ySplit: arr.yFixed }],
  }
  LOG('start generating workbook.....')
  const workbook = await exportExcelHelper.prepareExcelWorkbookFromArray({
    workbooks: sheets,
  })
  LOG('workbook generated.....')
  LOG(workbook)
  return workbook
}
