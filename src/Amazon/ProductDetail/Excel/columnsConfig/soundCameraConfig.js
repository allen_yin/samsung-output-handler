export default [
  {
    key: 'speaker',
    name: 'Speaker',
  },
  {
    key: 'audioType',
    name: 'Audio Type',
  },
  {
    key: 'soundEffect',
    name: 'Sound Effect',
  },
  {
    key: 'webCam',
    name: 'Web Cam',
  },
  {
    key: 'internalMic',
    name: 'Internal Mic',
  },
]
