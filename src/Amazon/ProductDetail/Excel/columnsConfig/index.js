import certificationConfig from './certificationConfig'
import dimensionsWeightConfig from './dimensionsWeightConfig'
import displayConfig from './displayConfig'
import expansionConfig from './expansionConfig'
import graphicsConfig from './graphicsConfig'
import includedInBoxConfig from './includedInBoxConfig'
import inputDeviceConfig from './inputDeviceConfig'
import memoryConfig from './memoryConfig'
import powerConfig from './powerConfig'
import processorConfig from './processorConfig'
import securitySafetyConfig from './securitySafetyConfig'
import soundCameraConfig from './soundCameraConfig'
import storageConfig from './storageConfig'
import wirelessConfig from './wirelessConfig'

const _getFeatureSubtitle = features => {
  let children = []
  for (var i = 0; i < features.length; i++) {
    children.push({
      key: features[i][0],
      name: features[i][0],
    })
  }
  return {
    key: 'features',
    name: 'Features',
    children,
  }
}

const _getSpecSubtitle = specifications => {
  let children = []
  for (var i = 0; i < specifications.length; i++) {
    specifications[i].values &&
      specifications[i].values.forEach(value => {
        children.push({
          key: value[0],
          name: value[0],
        })
      })
  }
  return {
    key: 'specifications',
    name: 'Specifications',
    children,
  }
}

export default ({ features, specifications }) => {
  let res = [
    {
      key: 'productTitle',
      name: 'Product Title',
    },
    {
      key: 'productDescription',
      name: 'Product Description',
    },
    {
      key: 'productStatus',
      name: 'Product Status',
    },
    {
      key: 'launchDate',
      name: 'Launch Date',
    },
    {
      key: 'materialCode',
      name: 'Material Code',
    },
    {
      key: 'modelCode',
      name: 'Model Code',
    },
    {
      key: 'projectName',
      name: 'Project Name',
    },
    {
      key: 'channelAssortment',
      name: 'Channel Assortment',
      default: 'AMAZON',
    },
    {
      key: 'ourPrice',
      name: 'Our Price',
    },
    {
      key: 'listPrice',
      name: 'List Price',
    },
    {
      key: 'color',
      name: 'Color',
    },
    {
      key: 'operatingSystem',
      name: 'Operating System',
    },
    // {
    //   key: 'display',
    //   name: 'Display',
    //   children: displayConfig,
    // },
    // {
    //   key: 'processor',
    //   name: 'Processor',
    //   children: processorConfig,
    // },
    // {
    //   key: 'storage',
    //   name: 'Storage',
    //   children: storageConfig,
    // },
    // {
    //   key: 'memory',
    //   name: 'Memory',
    //   children: memoryConfig,
    // },
    // {
    //   key: 'graphics',
    //   name: 'Graphics',
    //   children: graphicsConfig,
    // },
    {
      key: 'opticalDiskDrive',
      name: 'Optical Disk Drive',
    },
    // {
    //   key: 'soundCamera',
    //   name: 'Sound & Camera',
    //   children: soundCameraConfig,
    // },
    // {
    //   key: 'wireless',
    //   name: 'Wireless',
    //   children: wirelessConfig,
    // },
    // {
    //   key: 'expansion',
    //   name: 'Expansion',
    //   children: expansionConfig,
    // },
    // {
    //   key: 'inputDevice',
    //   name: 'Input Device',
    //   children: inputDeviceConfig,
    // },
    {
      key: 'countryOfOrigin',
      name: 'Country Of Origin',
    },
    // {
    //   key: 'power',
    //   name: 'Power',
    //   children: powerConfig,
    // },
    // {
    //   key: 'securitySafety',
    //   name: 'Security & Safety',
    //   children: securitySafetyConfig,
    // },
    // {
    //   key: 'certification',
    //   name: 'Certification',
    //   children: certificationConfig,
    // },
    // {
    //   key: 'includedInBox',
    //   name: 'Included In Box',
    //   children: includedInBoxConfig,
    // },
    {
      key: 'material',
      name: 'Material',
    },
    {
      key: 'upc',
      name: 'UPC',
    },
    // {
    //   key: 'dimensionsWeight',
    //   name: 'Dimensions & Weight',
    //   children: dimensionsWeightConfig,
    // },
    {
      key: 'warranty',
      name: 'Warranty',
    },
  ]
  // if (features && features.length !== 0) {
  //   res.push(_getFeatureSubtitle(features))
  // }
  if (specifications && specifications.length !== 0) {
    //res.push(_getSpecSubtitle(specifications))
    for (var i = 0; i < specifications.length; i++) {
      specifications[i].values &&
        specifications[i].values.forEach(value => {
          res.push({
            key: "specifications:" + value[0],
            name: value[0],
          })
        })
    }
  }
  return res
}
