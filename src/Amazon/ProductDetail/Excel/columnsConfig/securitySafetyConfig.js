export default [
  {
    key: 'fingerprintReader',
    name: 'Fingerprint Reader',
  },
  {
    key: 'tpm',
    name: 'TPM',
  },
  {
    key: 'freeFallSensor',
    name: 'Free Fall Sensor',
  },
  {
    key: 'securityLockSlot',
    name: 'Security Lock Slot',
  },
  {
    key: 'biosHDDPassword',
    name: 'BIOS/HDD Password',
  },
  {
    key: 'amt',
    name: 'AMT',
  },
]
