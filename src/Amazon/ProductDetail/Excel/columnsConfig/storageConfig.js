export default [
  {
    key: 'storageCapacity',
    name: 'Storage Capacity',
  },
  {
    key: 'storageTechnology',
    name: 'Storage Technology',
  },
  {
    key: 'storageInterface',
    name: 'Storage Interface',
  },
]
