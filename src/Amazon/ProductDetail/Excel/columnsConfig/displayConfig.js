export default [
  {
    key: 'displaySize',
    name: 'Display Size',
  },
  {
    key: 'resolution',
    name: 'Resolution',
  },
  {
    key: 'technology',
    name: 'Technology',
  },
  {
    key: 'aspectRatio',
    name: 'Aspect Ratio',
  },
  {
    key: 'brightness',
    name: 'Brightness',
  },
  {
    key: 'touchScreen',
    name: 'TouchScreen',
  },
]
