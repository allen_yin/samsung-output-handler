export default [
  {
    key: 'chipset',
    name: 'Chipset',
  },
  {
    key: 'dedicatedOrIntegrated',
    name: 'Dedicated or Integrated',
    width: 22,
  },
  {
    key: 'maxGraphicsMemory',
    name: 'Maximum Graphics Memory',
    width: 22,
  },
]
