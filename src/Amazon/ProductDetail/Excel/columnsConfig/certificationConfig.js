export default [
  {
    key: 'epeatRating',
    name: 'Epeat Rating',
  },
  {
    key: 'energyStarRating',
    name: 'Energy Star Rating',
  },
]
