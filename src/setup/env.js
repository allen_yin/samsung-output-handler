import dotenv from 'dotenv'

const ENV_FILE_MAP = {
  dev: '.dev.env',
  local: '.local.env',
}

const APP_ENV = process.env.APP_ENV || 'local'
const ENV_FILE = ENV_FILE_MAP[APP_ENV] || ENV_FILE_MAP['local']

dotenv.config({
  path: `${__dirname}/../../.env/${ENV_FILE}`
})

console.log(`APP_ENV: ${process.env.APP_ENV}`)
console.log(`NODE_ENV: ${process.env.NODE_ENV}`)
console.log(`PROXY: ${process.env.PROXY}`)
console.log(`DEBUG: ${process.env.DEBUG}`)
