import './setup'

import Amazon from './Amazon'
import Bestbuy from './Bestbuy'
import Walmart from './Walmart'


export default {
  Amazon,
  Bestbuy,
  Walmart
}
