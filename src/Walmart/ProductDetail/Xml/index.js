import debug from 'debug'
import js2xmlparser from 'xml-js'

const LOG = debug('output-handler-helpers')
let json
let clearEmpties = obj => {
  for (var k in obj) {
    if (!obj[k]) {
      delete obj[k] //object is null or undefined so delete it
    }
    if (typeof obj[k] !== 'object') {
      continue // If null or not an object, skip to the next iteration
    }
    // The property is an object
    clearEmpties(obj[k]) // <-- Make a recursive call on the nested object
    if (Object.keys(obj[k]).length === 0) {
      delete obj[k] // The object had no properties, so delete that property
    }
  }
  return obj
}
export default obj => {
  switch(obj.type){
    case 'TV':
      json = {
        _declaration: {
          _attributes: {
            version: '1.0',
            encoding: 'UTF-8',
            standalone: 'yes',
          },
        },
        SupplierItemFeed: {
          _attributes: {
            'xmlns:wal': 'http://walmart.com/',
          },
          SupplierItemFeedHeader: {
            version: obj.version || '3.2',
          },
          SupplierItem: {
            SupplierProduct: {
              SkuUpdate: obj.skuUpdate || 'Yes', //required
              sku: obj.samsungSku, //required
              msrp: obj.price || null,
              productName: { _cdata: obj.title }, //required}
              productIdentifiers: {
                productIdentifier: {
                  productIdType: obj.productIdType, //required
                  productId: obj.productId, //required
                },
              },
              category: {
                Electronics: {
                  TVsAndVideoDisplays: {
                    shortDescription: { _cdata: obj.shortDescription }, //required
                    brand: obj.brand || 'Samsung',
                    manufacturer: obj.manufacturer || 'Samsung',
                    manufacturerPartNumber: obj.manufacturerPartNumber || null,
                    mainImageUrl: { _cdata: obj.mainImageUrl },
                    isPrivateLabelOrUnbranded: obj.isPrivateLabelOrUnbranded, //required
                    isProp65WarningRequired: obj.isProp65WarningRequired, //required
                    hasStateRestrictions: obj.hasStateRestrictions, //required
                    stateRestrictions: obj.stateRestrictions || null,
                    isEnergyGuideLabelRequired: obj.isEnergyGuideLabelRequired, //required
                    displayTechnology: obj.displayTechnology || null,
                    keyFeatures: {
                      keyFeaturesValue: { _cdata: obj.keyFeatures || null },
                    },
                    screenSize: {
                      measure: obj.screenMeasure || null,
                      unit: obj.screenUnit || null,
                    },
                    modelNumber: obj.modelCode || null,
                    assembledProductHeight: {
                      measure: obj.heightMeasure || null,
                      unit: obj.heightUnit || null,
                    },
                    assembledProductWidth: {
                      measure: obj.widthMeasure || null,
                      unit: obj.widthUnit || null,
                    },
                    assembledProductWeight: {
                      measure: obj.weightMeasure || null,
                      unit: obj.weightUnit || null,
                    },
                    prop65WarningText: obj.prop65WarningText || null, //required
                    batteryTechnologyType: obj.batteryTechnologyType || null,
                    hasWarranty: obj.hasWarranty || null,
                    warrantyURL: { _cdata: obj.warrantyURL || null },
                    resolution: { _cdata: obj.resolution || null },
                    refreshRate: {
                      measure: obj.refreshMeasure || null,
                      unit: obj.refreshUnit || null,
                    },
                  },
                },
              },
            },
          },
        },
      }
      break;
    default:
      json = {
        _declaration: {
          _attributes: {
            version: '1.0',
            encoding: 'UTF-8',
            standalone: 'yes',
          },
        },
        SupplierItemFeed: {
          _attributes: {
            'xmlns:wal': 'http://walmart.com/',
          },
          SupplierItemFeedHeader: {
            version: obj.version || '3.2',
          },
          SupplierItem: {
            SupplierProduct: {
              SkuUpdate: obj.skuUpdate || 'Yes', //required
              sku: obj.samsungSku, //required
              msrp: obj.price || null,
              productName: { _cdata: obj.title }, //required}
              productIdentifiers: {
                productIdentifier: {
                  productIdType: obj.productIdType, //required
                  productId: obj.productId, //required
                },
              },
              category: {
                Electronics: {
                  TVsAndVideoDisplays: {
                    shortDescription: { _cdata: obj.shortDescription }, //required
                    brand: obj.brand || 'Samsung',
                    manufacturer: obj.manufacturer || 'Samsung',
                    manufacturerPartNumber: obj.manufacturerPartNumber || null,
                    mainImageUrl: { _cdata: obj.mainImageUrl },
                    isPrivateLabelOrUnbranded: obj.isPrivateLabelOrUnbranded, //required
                    isProp65WarningRequired: obj.isProp65WarningRequired, //required
                    hasStateRestrictions: obj.hasStateRestrictions, //required
                    stateRestrictions: obj.stateRestrictions || null,
                    isEnergyGuideLabelRequired: obj.isEnergyGuideLabelRequired, //required
                    displayTechnology: obj.displayTechnology || null,
                    keyFeatures: {
                      keyFeaturesValue: { _cdata: obj.keyFeatures || null },
                    },
                    screenSize: {
                      measure: obj.screenMeasure || null,
                      unit: obj.screenUnit || null,
                    },
                    modelNumber: obj.modelCode || null,
                    assembledProductHeight: {
                      measure: obj.heightMeasure || null,
                      unit: obj.heightUnit || null,
                    },
                    assembledProductWidth: {
                      measure: obj.widthMeasure || null,
                      unit: obj.widthUnit || null,
                    },
                    assembledProductWeight: {
                      measure: obj.weightMeasure || null,
                      unit: obj.weightUnit || null,
                    },
                    prop65WarningText: obj.prop65WarningText || null, //required
                    batteryTechnologyType: obj.batteryTechnologyType || null,
                    hasWarranty: obj.hasWarranty || null,
                    warrantyURL: { _cdata: obj.warrantyURL || null },
                    resolution: { _cdata: obj.resolution || null },
                    refreshRate: {
                      measure: obj.refreshMeasure || null,
                      unit: obj.refreshUnit || null,
                    },
                  },
                },
              },
            },
          },
        },
      }
      break;
  }
  let options = {
    compact: true,
    ignoreComment: true,
    spaces: 4,
    indentCdata: true,
  }
  let res = clearEmpties(json)
  let result = js2xmlparser.json2xml(res, options)
  result = result.replace(/]]>\n/g, ']]>')
  return result
}
