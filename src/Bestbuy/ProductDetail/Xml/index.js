import debug from 'debug'
import js2xmlparser from 'xml-js'

const LOG = debug('output-handler-helpers')

const nestElement = (arr, res = [], field) => {
  console.log(typeof arr, arr, arr.length, field)
  for (var i = 0; i < arr.length; i++) {
    Object.keys(arr[i]).forEach(key => {
      if (typeof arr[i] !== 'object') {
        res.push(`${arr[i][key]}`)
      } else {
        res.push(nestObjElement(arr[i], (res = []), field))
      }
    })
  }
  return res
}

const nestArrObj = (arr, res = [], field) => {
  if (!arr) {
    return
  }
  for (var i = 0; i < arr.length; i++) {
    Object.keys(arr[i]).forEach(key => {
      res.push(`${arr[i][key]}`)
    })
  }
  return res
}

const attrGrp = (arr, res = []) => {
  if (!arr) {
    return
  }
  let obj

  for (var i = 0; i < arr.length; i++) {
    obj = {
      Name: arr[i]['keys'],
      Attribute: [],
    }
    arr[i]['values'].forEach(value => {
      let ob = {}
      ob['Name'] = value[0]
      ob['Value'] = value[1]
      obj['Attribute'].push(ob)
    })
    res.push(obj)
  }
  return res
}

const nestObjElement = (obj, res = [], field) => {
  Object.keys(obj).forEach(key => {
    res.push(`${key}:${obj[key]}`)
  })
  return res
}
const prdGrp = (arr, res = []) => {
  if (!arr) {
    return
  }
  for (var i = 0; i < arr.length; i++) {
    let obj = {
      Header: arr[i][0],
      Text: arr[i][1],
    }
    res.push(obj)
  }

  return res
}

var emptyObj = ''
export default jsonData => {
  let structuredJson = {
    _declaration: {
      _attributes: {
        version: jsonData.version || '1.0',
        encoding: jsonData.encoding || 'UTF-8',
        standalone: jsonData.standalone || 'yes',
      },
    },
    Feed: {
      _attributes: {
        language: jsonData.language || 'en',
        market: jsonData.market || 'us',
      },
      Product: {
        PartNumber: jsonData.sku,
        ProductDescription: jsonData.shortDescription,
        Name: jsonData.name,
        UpcEan: jsonData.upc,
        LaunchDate: jsonData.launchDate,
        EndOfLifeDate: jsonData.endOfLifeDate,
        HeroImage: jsonData.images[0],
        Images: {
          Image: jsonData.images,
        },
        EnergyLabelImages: jsonData.energyLabelImages || emptyObj,
        LogoItems: jsonData.logoItems,
        KeySellingPoints: {
          item: nestArrObj(jsonData.includedItemList, [], 2), //whats included
        },

        ProductFeatures: {
          Item: prdGrp(jsonData.features, []),
        },
        AttributeGroup: attrGrp(jsonData.specifications, [], 2),
        PdfProductDataSheet: jsonData.pdfProductDataSheet,
        PdfUserManual: jsonData.pdfUserManual,
        PdfQuickStartGuide: jsonData.pdfQuickStartGuide,
        WarrantyCard: jsonData.warrantyCard,
        InstallationGuide: jsonData.installationGuide,
        PdfFTCEnergyGuide: jsonData.pdfFTCEnergyGuide,
        PdfFTCLightingFacts: jsonData.pdfFTCLightingFacts,
        ProductBrochures: jsonData.productBrochures,
        PdfMsds: jsonData.pdfMsds,
        PdfEUEnergyLabel: jsonData.pdfEUEnergyLabel,
        AccessoriesPartNumbers: jsonData.accessoriesPartNumbers,
        MainProductsPartNumbers: jsonData.mainProductsPartNumbers,
        Videos: jsonData.videos,
        Footnotes: jsonData.footnotes,
        LegalCopy: jsonData.legalCopy,
      },
    },
  }
  console.log(jsonData.productFeatures)
  let options = {
    compact: true,
    ignoreComment: true,
    spaces: 4,
  }
  let resultXml = js2xmlparser.json2xml(structuredJson, options)
  return resultXml
}
