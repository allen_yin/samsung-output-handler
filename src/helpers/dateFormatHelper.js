import Moment from 'moment'

export const formatDateByNumber = date => {
  if (date) return Moment(date, 'YYYYMMDD').format('MMM DD, YYYY')
  return 'N/A'
}

export const exportDateFormat = date => {
  if (date) return Moment(date, 'YYYYMMDD').format('YYYY-MM-DD')
  return 'N/A'
}
