import { DOT_SPLITER, SPACE_SPLITER } from '../components/constants/attachment'

export const capitalize = str => {
  return str.charAt(0).toUpperCase() + str.slice(1)
}

export const removeLastChar = str => {
  if (str.length > 0) {
    return str.substr(0, str.length - 1)
  }
}

export const toUnderscore = str => {
  if (str && str.length > 1) {
    String.prototype.toUnderscore = function() {
      return this.replace(/([A-Z])/g, function($1) {
        return '_' + $1.toLowerCase()
      })
    }

    return str.toUnderscore()
  }
}

export const phoneNumberFormat = num => {
  let numVal = num ? num : ''
  let numStr = numVal
    .split(' ')
    .join('')
    .split('-')
    .join('')
    .split('(')
    .join('')
    .split(')')
    .join('')

  if (numStr.length !== 10 || !/^\d+$/.test(numStr)) return ''
  else
    return (
      '(' +
      numStr.slice(0, 3) +
      ') ' +
      numStr.slice(3, 6) +
      '-' +
      numStr.slice(6, numStr.length)
    )
}

export const fileNameAndExtension = (name = '') => {
  const arr = name.split('.')
  let fileName = arr.slice(0, arr.length - 1).join('.')
  let extension = arr[arr.length - 1]
  return {
    fileName,
    extension,
  }
}

export const fileNameEncoding = (fileName = '') => {
  return fileName
    .split('.')
    .join(DOT_SPLITER)
    .split(' ')
    .join(SPACE_SPLITER)
}

export const fileNameDecoding = (fileName = '') => {
  return fileName
    .split(DOT_SPLITER)
    .join('.')
    .split(SPACE_SPLITER)
    .join(' ')
}

export const getFileType = (fileId = '') => {
  let arr = fileId.split('.')
  return arr[arr.length - 1]
}

export const replaceAll = (str, find, replace) => {
  return str.replace(new RegExp(find, 'g'), replace)
}

export const defaultDashValue = value =>
  value || value === 0 || value === false ? value : '-'

export const getCapitalizedString = (str = '') => {
  str = str.trim()
  let result = ''
  const arr = str.split(' ')
  arr.forEach(item => {
    result += capitalize(item.toLowerCase()) + ' '
  })
  return result
}

export const capFirstLetter = str => str.charAt(0).toUpperCase() + str.slice(1)

export const strEllipsis = (str, maxLen, trunc = maxLen - 2) =>
  str && str.length > maxLen ? `${str.substring(0, trunc)}...` : str

export const getUrlParam = (url = '') => {
  url = url.split('?').join('')
  const paramsArr = url.split('&')
  const params = {}
  paramsArr.forEach(param => {
    let arr = param.split('=')
    params[arr[0]] = arr[1]
  })
  return params
}

export const jsonStringWithoutQuotes = (obj = {}) => {
  return JSON.stringify(obj).replace(/"(\w+)"\s*:/g, '$1:')
}

export const ipStringValidate = (str = '') => {
  if (
    /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(
      str
    )
  )
    return true
  return false
}

export const safelyDecodeString = note => {
  try {
    return decodeURIComponent(note)
  } catch (e) {
    return note
  }
}
