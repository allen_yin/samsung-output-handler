import debug from 'debug'
import Excel from 'exceljs/dist/es5/exceljs.browser'

const LOG = debug('output-handler-helpers')

const getExcelColumnName = num => {
  for (var ret = '', a = 1, b = 26; (num -= a) >= 0; a = b, b *= 26) {
    ret = String.fromCharCode(parseInt((num % b) / a) + 65) + ret
  }
  return ret
}

const prepareExcelWorkbookFromArray = async params => {

  const { workbooks } = params
  let workbook = new Excel.Workbook()
  for (let wbName in workbooks) {
    if (!workbooks.hasOwnProperty(wbName)) continue
    workbook = prepareExcelSheetFromArray({
      autoFilter: workbooks[wbName]['autoFilter'],
      columns: workbooks[wbName]['columns'],
      data: workbooks[wbName]['data'],
      heights: workbooks[wbName]['heights'],
      mergeCells: workbooks[wbName]['mergeCells'],
      name: wbName,
      styles: workbooks[wbName]['styles'],
      views: workbooks[wbName]['views'],
      workbook,
    })
  }
  return workbook
}

function prepareExcelSheetFromArray(params) {
  const {
    autoFilter,
    columns,
    data,
    heights,
    mergeCells,
    name,
    styles,
    views,
    workbook,
  } = params
  var sheet = workbook.addWorksheet(name)
  LOG(`sheet ${name} added....`)
  sheet.autoFilter = autoFilter
  LOG('autoFilter added.....')
  sheet.columns = columns
  LOG('columns added.....')
  sheet.addRows(data)
  LOG('rows added.......')
  sheet.views = views
  styles !== undefined &&
    Object.keys(styles).forEach(cellKey => {
      sheet.getCell(cellKey).numFmt = styles[cellKey].numFmt
      sheet.getCell(cellKey).font = styles[cellKey].font
      sheet.getCell(cellKey).alignment = styles[cellKey].alignment
      sheet.getCell(cellKey).border = styles[cellKey].border
      sheet.getCell(cellKey).fill = styles[cellKey].fill
    })

  mergeCells !== undefined &&
    Object.keys(mergeCells).forEach(merge => {
      sheet.mergeCells(merge)
    })
  heights !== undefined &&
    heights.forEach(rowConfig => {
      sheet.getRow(rowConfig.row).height = rowConfig.height
    })
  LOG('workbook ready')
  return workbook
}

const subHeaderStyle = {
  // font: {
  //   bold: true,
  // },
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } },
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00F6F7F8' },
  },
}

const topHeaderStyle = {
  font: {
    size: 13,
    bold: true,
  },
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } },
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00F6F7F8' },
  },
}

const evenCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } },
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00F6F7F8' },
  },
}

const oddCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
    wrapText: true
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } },
  },
}

const warningCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } },
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00FFFF00' },
  },
}

const dangerCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } },
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00EF5350' },
  },
}

const goodCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } },
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '0000C853' },
  },
}

const greatCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } },
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '0029B6F6' },
  },
}

export const styles = {
  subHeaderStyle,
  topHeaderStyle,
  evenCellStyle,
  oddCellStyle,
  warningCellStyle,
  dangerCellStyle,
  goodCellStyle,
  greatCellStyle,
}

const exportXLSXHelper = {
  getExcelColumnName,
  prepareExcelWorkbookFromArray,
}

export default exportXLSXHelper
