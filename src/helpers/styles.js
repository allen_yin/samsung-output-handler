export const subHeaderStyle = {
  font: {
    bold: true,
  },
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } },
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00F6F7F8' },
  },
}

export const topHeaderStyle = {
  font: {
    size: 13,
    bold: true,
  },
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } },
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00F6F7F8' },
  },
}

export const evenCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } },
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00F6F7F8' },
  },
}
export const oddCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } },
  },
}

export const warningCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } },
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00FFFF00' },
  },
}

export const dangerCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } },
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '00EF5350' },
  },
}

export const goodCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } },
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '0000C853' },
  },
}

export const greatCellStyle = {
  alignment: {
    vertical: 'middle',
    horizontal: 'center',
  },
  border: {
    top: { style: 'thin', color: { argb: '00D3D3D3' } },
    bottom: { style: 'thin', color: { argb: '00D3D3D3' } },
    right: { style: 'thin', color: { argb: '00D3D3D3' } },
    left: { style: 'thin', color: { argb: '00D3D3D3' } },
  },
  fill: {
    type: 'pattern',
    pattern: 'solid',
    fgColor: { argb: '0029B6F6' },
  },
}
