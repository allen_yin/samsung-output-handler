export const undefOrNull = thing => thing === undefined || thing === null
export const notUndefOrNull = thing => !undefOrNull(thing)
