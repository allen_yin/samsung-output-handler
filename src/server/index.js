import '../setup/server.js'
import debug from 'debug'
import fs from 'fs'
import structuredJson from './bbuUserJsonTpl'
import testSheet from '../index'

const testData = {
  modelCode: 'B000SX4C7Q',
  productDescription:
    'Samsung Genuine Refrigerator Water Filters give your family clean, fresh water, while protecting your refrigerator and home from potential damage. This filter uses a powerful concentrated carbon filter to eliminate over 99 percent of potentially harmful contaminants. Samsung Genuine filters are produced in both Korea and Mexico. Shipping Note: Shipping to PO Boxes and APO addresses is not available for this item.',
  productTitle: 'Samsung Genuine DA29-00003G Refrigerator Water Filter, 1 Pack',
  launchDate: 'May 22, 2004',
  dimensionsWeight: {
    unitPackageWeight: '11.5 ounces',
  },
  ourPrice: '$37.90',
  listPrice: null,
  features: [
    [
      'Fast print speeds up to 15 ISO ppm* in black',
      'Creates the 1st page out in as little as 8.5 seconds* for rapid document production.------test-----\n--------',
    ],
    [
      'Be sure you have the right cartridge',
      'Enter your printer&apos;s model number into our Ink and Toner Finder to find compatible cartridges. <a href="/site/printers/printer-ink-toner/abcat0807000.c?id=abcat0807000" target="_blank">Get started &#x203A;</a>',
    ],
    [
      'Built-in wireless LAN (802.11b/g/n)',
      'Ensures fast data transfer from a wireless connection.',
    ],
    [
      '400MHz processor and 64MB memory',
      'Offer improved performance when printing complex documents.',
    ],
  ],
  specifications: [
    {
      keys: 'Key Specs',
      values: [
        ['ENERGY STAR Certified', 'Yes'],
        ['Printer Type', 'Printer'],
        ['Duty Cycle', 'Up to 10,000'],
        ['ISO Mono Print Speed', '15 pages per minute'],
        ['Monochromatic/Color', 'Monochromatic'],
      ],
    },
    {
      keys: 'Connectivity',
      values: [
        ['Printer Connectivity', 'Wi-Fi, USB 2.0, Google Cloud Print'],
        [
          'Minimum System Requirements',
          'PC: Windows 2003, 2008, 2008 R2, Server 2012, XP, Vista, 7 or 8 (32- or 64-bit); Mac: OS X 10.5 - 10.9; Linux',
        ],
      ],
    },
  ],
};
const walmartTest = {

  sku:"442B9F0LA4SZ",
  productId:"442B9F0LA4SZ",
  shortDescription:"Introducing The Frame from Samsung - a revolutionary way to think about your TV. When you're not watching the brilliant 4K UHD TV with HDR, it turns into a beautiful work of art on your wall. With an array of paintings, prints, photos, and frames to suit every mood and style, The Frame TV goes beyond entertainment and becomes an expression of you",
  manufacturerPartNumber:"UN55LS003AFXZA"
};

const LOG = debug('output-handler-main')
const fileName = 'test-amazon-pdetail.xlsx'
const main = async () => {
  LOG('testing .............')
  try {
    var jsonData = structuredJson
    const xml = await testSheet.Bestbuy.ProductDetail.Xml(jsonData)
    fs.writeFile('storage/xml/besyBuyXML.xml', xml, (err, docs) => {
      LOG('bestbuy write is done.....')
    })

    const walmartXML = await testSheet.Walmart.ProductDetail.Xml(walmartTest);
    fs.writeFile('storage/xml/walmartXML.xml', walmartXML, (err,docs)=>{
      LOG('walmart write is done.....')
    })

    const workbook = await testSheet.Amazon.ProductDetail.Excel({
      productInfo: testData,
    })
    workbook.xlsx.writeFile('storage/xlsx/' + fileName).then(() => {
      LOG('write file is done.....')
    })
  } catch (e) {
    LOG(e)
  }

  LOG(res)
}

LOG('start main....')

main()
